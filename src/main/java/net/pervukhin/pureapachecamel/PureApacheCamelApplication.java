package net.pervukhin.pureapachecamel;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class PureApacheCamelApplication {

    public static void main(String[] args) {
        SpringApplication.run(PureApacheCamelApplication.class, args);
    }
}
