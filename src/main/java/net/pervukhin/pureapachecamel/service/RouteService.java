package net.pervukhin.pureapachecamel.service;

import org.apache.camel.CamelContext;
import org.apache.camel.builder.RouteBuilder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.event.ApplicationReadyEvent;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Service;

@Service
public class RouteService {
    private static final Logger logger = LoggerFactory.getLogger(RouteService.class);
    @Autowired
    private CamelContext camelContext;

    @EventListener(ApplicationReadyEvent.class)
    public void init() throws Exception {
        camelContext.addRoutes(new RouteBuilder() {

            @Override
            public void configure() throws Exception {
                from("direct:tuneBPMN")
                        .bean("restExecutorService", "doRestRequest")
                        .bean("restExecutorService", "doRestRequest");
            }
        });

        logger.info("Маршрут настроен");
    }
}
